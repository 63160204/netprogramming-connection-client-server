import socket

s = socket.socket()        

port = 12345

s.connect(('127.0.0.1',port))

print ("Socket successfully created")

number = 1
while number < 100:
    data = s.recv(1024).decode()
    if not data:
        break

    number = int(data)
    print(number)

    number += 1
    s.send(str(number).encode())

s.close()
